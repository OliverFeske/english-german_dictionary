#pragma once

#include "windows.h"

#include <vector>
#include <iostream>

struct Entry
{
	int idx;
	std::string word;
	std::string translation;
	std::string englishDescription;
};

class Dictionary
{
public:
	Dictionary() {}
	~Dictionary() {}

public:
	bool Setup(HANDLE hConsole);
	bool MainLoop();

protected:
	void DisplayMainOptions();
	void ProcessMainInput(char key);
	void FindWords();
	void DisplayFoundWords();
	void CharToUpper(char& lower);
	void AddWord();
	void SelectWordAtIndex();
	void DisplaySelectedWordInfo();
	void ProcessSelectedWordInput(char key);
	bool ConvertLinesToEntry(Entry& inEntry, std::vector<std::string>& inLines, std::string& idxDelimiter);
	void ConvertEntryToLines(Entry& inEntry, std::vector<std::string>& inLines);
	void SortFoundEntries();
	void cls();

protected:
	std::string lowerChars{ "abcdefghijklmnopqrstuvwxyz" };
	std::string upperChars{ "ABCDEFGHIJKLMNOPQRSTUVWXYZ" };

	std::string currentInput{ "" };
	std::vector<Entry> fittingEntries{};

	Entry currentSelectedWord{};

	bool bQuit{ false };

	HANDLE console{};
	unsigned currentWordCount{ 0 };
};